import { useState } from "react";
import "./App.css";
import dataGlass from "./dataGlasses.json";
import GlassList from "./Glasses/GlassList";
import Model from "./Glasses/Model";

const data = {
  id: "",
  price: "",
  name: "",
  url: "",
  desc: "",
};

function App() {
  const [glass, setGlass] = useState(data);
  return (
    <div className="App">
      <div style={{
          backgroundImage: "url(./img/bg-img.jpg)",
          backgroundSize: "2000px",
          minHeight: "2000px",
        }}>
        <div style={{ backgroundColor: "rgba(0,0,0,.65)", minHeight: "2000px" }}>
          <h3
            style={{ backgroundColor: "rgba(0,0,0,.3)" }}
            className="text-center text-light p-5"
          >
            Try Glasses App
          </h3>
        
             
          
                  <div className="container">
                    <Model glass={glass} />
                  </div>
           
                    <GlassList dataGlass={dataGlass} setGlass={setGlass} />
                  
            
           
        
        
        </div>
            </div>
      </div>
  );
}

export default App;
