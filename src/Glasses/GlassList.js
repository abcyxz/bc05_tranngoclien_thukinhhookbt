import React, { memo } from "react";

function GlassList({ dataGlass, setGlass }) {
    const x = 75;
    const y = 200;
    const style ={transform:`translate(${x}px, ${y}px)`}
  return (
   
    <div className="container mt-5 d-flex bg-light "
    style={style}
   
    >
      {dataGlass.map((item, index) => {
        return (
          <div key={index} className="">
            <img
              style={{ cursor: "pointer" }}
              className="img-fluid"
              src={item.url}
              onClick={() => setGlass(item)}
              alt=""
            />
          </div>
        );
      })}
    </div>
  );
}

export default memo(GlassList);
