import React from "react";

export default function Model({ glass }) {
  const { url, desc, name, price, id } = glass;

  return (
   
        <div className="row mt-5 text-center">
            <div className="col-6">
              <div className="position-relative">
                <img
                  style={{ width: "250px" }}
                  className="position-absolute"
                  src="./glassesImage/model.jpg"
                  alt="model.jpg"
                />
              </div>
              <img
                src={url}
                className="position-absolute "
                style={{
                  width: "140px",
                  top: "75px",
                  right: "90px",
                  opacity: "0.7",
                  animation: `animChangeGlass${Date.now()} 1s`,
                }}
        
              />
              <div
                style={{
                  width: "250px",
                  top: "200px",
                  left: "270px",
                  paddingLeft: "15px",
                  backgroundColor: "rgba(255,127,0,.5)",
                  textAlign: "left",
                  height: "105px",
                }}
                className="position-relative"
              >
                <span
                  style={{ color: "#AB82FF", fontSize: "15px" }}
                  className="font-weight-bold"
                >
                  {id}: 
                  {name}
                </span>
                <span style={{ color: "green", marginLeft: "50px", fontSize: "12px" }}>
                  ${price}
                </span>
                <br />
                <span
                  style={{ fontSize: "10px", marginRight: "5px", fontWeight: "400" }}
                >
                  {desc}
                </span>
              </div>
            </div>
            <div className="col-6">
                <img
                  style={{ width: "250px" }}
                  className="position-absolute"
                  src="./glassesImage/model.jpg"
                  alt="model.jpg"
                />
              </div>
    
        
    </div>
    
  );
}
